var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass')(require('sass'));
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var debug = require('gulp-debug');
var unicode = require('gulp-sass-unicode');
var pump = require('pump');

gulp.task('styles', function () {
    return gulp.src(['../scss/style.scss'])
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                outputStyle: 'compressed'
            }).on(
                'error', sass.logError
            )
        )
        .pipe(unicode())
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 22 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('../'))
});

gulp.task('scripts', function () {
    return pump([
        //gulp.src('../js/*[!.min].js', { base: './' }),
        gulp.src(['../js/script.js', '../js/admin.js'], { base: './' }),
        //gulp.src(['../js/*.js', '!../js/*.min.js'], { base: './' }),
        debug(),
        rename({
            suffix: '.min'
        }),
        uglify(),
        gulp.dest('./')
    ]);
});

//Watch task
gulp.task('theme-watcher', function () {
    gulp.watch(['../scss/**/*.scss'], gulp.series('styles'));
    gulp.watch(['../js/*.js', '!../js/*.min.js'], gulp.series('scripts'));
});